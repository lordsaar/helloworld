//
//  helloWorldApp.swift
//  helloWorld
//
//  Created by Roland Schaar on 07.02.21.
//

import SwiftUI

@main
struct helloWorldApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
