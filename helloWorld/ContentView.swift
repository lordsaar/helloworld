//
//  ContentView.swift
//  helloWorld
//
//  Created by Roland Schaar on 07.02.21.
//

import SwiftUI

struct ContentView: View {
    @State var isTextShowing = true
    var body: some View {
        VStack {
            if isTextShowing {
                Text("Hello, world! Xcode 12 Branch")
                    .padding()
                    .font(.title)
            } else {
                Text("Roland for President").padding()
            }
            Button(action: {
                isTextShowing.toggle()
            }) {
                Text("Button")
            }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
